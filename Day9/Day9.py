inputs = []
for line in open('input.txt'):
    inputs.append([int(linepart) for linepart in line.rstrip()])

def get_adjacent_points(x,y):
    return [(x-1,y), (x+1,y), (x,y-1), (x,y+1)]

def check_borders(x,y):
    return (x >= 0 and x < len(inputs) and
            y >= 0 and y < len(inputs[0]))

def get_adjacent(x, y):
    return [inputs[rx][ry] for (rx,ry) in get_adjacent_points(x,y)
            if check_borders(rx,ry)]

def calc_basin_size(x,y):
    result = set()
    def is_valid_basin_point(x,y):
        return (not (x,y) in result and
                check_borders(x,y) and
                inputs[x][y] != 9)
    def get_basin_points(x,y):
        return [(adjx, adjy) for (adjx, adjy) in get_adjacent_points(x,y)
                if is_valid_basin_point(adjx, adjy)] 
    def calc_impl(x,y):
        result.add((x,y))
        for px, py in get_basin_points(x,y):
            calc_impl(px,py)
    calc_impl(x,y)
    return len(result)

lowpoints = []
basins = []
for x in range(0, len(inputs)):
    for y in range(0, len(inputs[0])):
        if not [lower for lower in get_adjacent(x,y) if inputs[x][y] >= lower]:
            lowpoints.append(inputs[x][y])
            basins.append(calc_basin_size(x,y))

result1 = sum(lowpoints) + len(lowpoints)
print(result1)

basins.sort()
from functools import reduce
result2 = reduce(lambda x,y: x*y, basins[-3:])
print(result2)