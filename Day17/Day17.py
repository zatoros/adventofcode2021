import re

x1, x2, y1, y2 = [int(num) for num in 
                  re.search("target area: x=(-?\d+)\.\.(-?\d+), y=(-?\d+)\.\.(-?\d+)",
                  open('input.txt').readline().rstrip()).groups()]

def is_in_area(x, y):
    return (x >= x1 and x <= x2 and
            y >= y1 and y <= y2)

def is_over_area(x, y, sx):
    return ((sx == 0 and (x < x1 or x > x2 or y < y2)) or 
            (x > x2))

def will_reach_area(sx, sy):
    x, y = 0, 0
    while not is_over_area(x,y,sx):
        x += sx
        y += sy
        if sx > 0: sx -= 1
        sy -= 1
        if is_in_area(x,y):
            return True
    else:
        return False

def highpoint(p):
    return (p+1)*p//2

def find_xlimits():
    xlimits = []
    for x in range(1, x2+1):
        if any(shot for shot in ((x+(x-i))*(i+1)/2 for i in range(0,x)) if shot >= x1 and shot <= x2):
            xlimits.append(x)
    return xlimits

result = []
for xx in find_xlimits():
    for yy in range(y1, abs(y1)):
        if will_reach_area(xx,yy):
            result.append((xx,yy))

print(highpoint(min(result,key=lambda xy : xy[1])[1]))
print(len(result))