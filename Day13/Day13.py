import re

ifile = open('input.txt')

lines = ifile.readlines()
paper = {(int(x), int(y)) for (x,y) in 
         [pointline.rstrip().split(',') for pointline in lines[:lines.index('\n')]]}
instructions = [(axis, int(val)) for (axis,val) in 
                [re.search('fold along (\w)=(\d+)',instline).groups()[:2] for instline in lines[lines.index('\n')+1:]]]

def print_paper(paper):
    for x in range(0,max(paper,key = lambda p : p[1])[1]+1):
        for y in range(0,max(paper,key = lambda p : p[0])[0]+1):
            print('#' if (y,x) in paper else '.',end='')
        print()

def fold_y(paper, fy):
    folded = {(tf[0], 2*fy-tf[1]) for tf in paper if tf[1] > fy}
    return {tf for tf in paper if tf[1] < fy} | folded

def fold_x(paper, fx):
    folded = {(2*fx-tf[0], tf[1]) for tf in paper if tf[0] > fx}
    return {tf for tf in paper if tf[0] < fx} | folded

def process_inst(paper, inst):
    if inst[0] == 'y':
        return fold_y(paper,inst[1])
    elif inst[0] == 'x':
        return fold_x(paper,inst[1])

paper = process_inst(paper, instructions.pop(0))
print(len(paper))

while instructions:
    paper = process_inst(paper, instructions.pop(0))

import sys
sys.stdout = open('output.txt', 'w')
print_paper(paper)