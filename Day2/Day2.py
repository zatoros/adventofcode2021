import re

inputs1 = {}
aim = 0
result2 = [ 0, 0 ]
for line in open('input.txt'):
    command, valuestr = re.search("(\w+) (\d+)", line).groups()
    value = int(valuestr)
    if command in inputs1:
        inputs1[command] += value
    else:
        inputs1[command] = value
    if command == 'forward':
        result2[0] += value
        result2[1] += aim*value
    else:
        aim += value if command == 'down' else -value

result1 = inputs1['forward'] * ( inputs1['down'] - inputs1['up'] )
print(result1)

result2 = result2[0]*result2[1]
print(result2)