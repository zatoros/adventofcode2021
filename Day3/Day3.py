lines = [line.rstrip() for line in open('input.txt')]

inputs = []
for line in lines:
    for i in range(0,len(line.rstrip())):
        if i == len(inputs): inputs.append(0) 
        inputs[i] += 1 if line[i] == '1' else -1

gamma = 0
epsilon = 0
bitlength = len(inputs)
for i in range(0, bitlength):
    value = inputs[i]
    bitnum = bitlength - i - 1
    if value > 0:
        gamma |= (1 << bitnum)  
    else:
        epsilon |= (1 << bitnum)

print(gamma * epsilon)

def get_ones_zeroes(bitlines, bitnum):
    return { one for one in bitlines if one[bitnum] == '1'}, {zero for zero in bitlines if zero[bitnum] == '0'}

oxygen = set(lines)
bitnum = 0
while len(oxygen) != 1:
    ones, zeroes = get_ones_zeroes(oxygen, bitnum)
    oxygen -= zeroes if len(ones) >= len(zeroes) else ones
    bitnum += 1

co2 = set(lines)
bitnum = 0
while len(co2) != 1:
    ones, zeroes = get_ones_zeroes(co2, bitnum)
    co2 -= ones if len(zeroes) <= len(ones) else zeroes
    bitnum += 1
    
print(int(oxygen.pop(),2)*int(co2.pop(),2))
