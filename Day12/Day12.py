import re

def duplicate_edges(edges):
    return edges + [edge[::-1] for edge in edges 
                    if edge[0] != 'start' and edge[1] != 'end']

def find_paths(edges, limit=1):
    result = []
    def find_paths_impl(edge, path, visited_nodes, edges):
        if (edge[1] == 'start' or 
            visited_nodes.get(edge[1],0) >= (1 if limit in visited_nodes.values() else limit)):
            return
        path.append(edge)
        if edge[1].islower():
            visited_nodes.setdefault(edge[1], 0)
            visited_nodes[edge[1]] += 1
        if edge[1] == 'end':
            result.append(path)
            return
        for nextedge in [nedge for nedge in edges if nedge[0] == edge[1]]:
            find_paths_impl(nextedge, path[:], visited_nodes.copy(), edges)
    for startedge in [edge for edge in edges if edge[0] == 'start']:
        find_paths_impl(startedge, [], {}, edges)
    return result

edges = [re.search('(\w+)-(\w+)',line).groups()[:2] for line in open('input.txt')]
edges = duplicate_edges(edges)

print(len(find_paths(edges)))
print(len(find_paths(edges, 2)))