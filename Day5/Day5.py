import re

def put_in_hydromap(hydromap, point):
    if point in hydromap:
        hydromap[point] += 1
    else:
        hydromap[point] = 1

def count_result(hydromap):
    return len([point for point in hydromap if hydromap[point] > 1])

hydromap1 = {}
hydromap2 = {}
for line in open('input.txt'):
    x1, y1, x2, y2 = [int(p) for p in re.search('(\w+),(\w+) -> (\w+),(\w+)', line).groups()]
    stepy = 1 if y1 < y2 else -1 if y1 > y2 else 0
    stepx = 1 if x1 < x2 else -1 if x1 > x2 else 0
    rangex = range(x1, x2 + stepx, stepx) if stepx != 0 else None
    rangey = range(y1, y2 + stepy, stepy) if stepy != 0 else None
    if stepx == 0:
        for y in rangey:
            put_in_hydromap(hydromap1, (x1,y))
            put_in_hydromap(hydromap2, (x1,y))
    elif stepy == 0:        
        for x in rangex:
            put_in_hydromap(hydromap1, (x,y1))
            put_in_hydromap(hydromap2, (x,y1))
    else:
        for x,y in zip(rangex, rangey):
            put_in_hydromap(hydromap2, (x,y))
        
print(count_result(hydromap1))
print(count_result(hydromap2))
