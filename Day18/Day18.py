from math import floor, ceil
from copy import deepcopy
from functools import reduce as freduce

inputs = []
def init_inputs():
    exec('global inputs\ninputs = ['+','.join(line.rstrip() for line in open('input.txt'))+']')

def magnitute(num):
    if isinstance(num,int):
        return num
    else:
        return 3*magnitute(num[0]) + 2*magnitute(num[1])

def get_num(num, indices):
    result = num[indices[0]]
    for index in indices[1:]:
        result = result[index]
    return result

def add_num(num, indices, toadd):
    numpair = get_num(num, indices[:-1])
    numpair[indices[-1]] += toadd

def num_split(num, indices):
    numpair = get_num(num, indices[:-1])
    divided = numpair[indices[-1]]/2
    numpair[indices[-1]] = [floor(divided), ceil(divided)]

def set_num(num, indices, val):
    get_num(num,indices[:-1])[indices[-1]] = val

class action:
    def endstate(self):
        return False
    def __call__(self, num, indices):
        fcnresult = self.fcn(num, indices)
        if fcnresult is None:
            if not self(num, indices + [0]):
                return self(num, indices + [1])
            return True
        return fcnresult    
    def process(self, num):
        if not self(num, [0]):
            return self(num, [1]) or self.endstate()
        return True

def reduce(num):
    class split(action):
        def fcn(self, num, indices):
            gotnum = get_num(num, indices)
            if isinstance(gotnum, int):
                if gotnum >= 10:
                    num_split(num, indices)
                    return True
                return False
    class explode(action):
        def __init__(self):
            self.leftint = self.right = None
        def fcn(self, num, indices):
            gotnum = get_num(num, indices)
            if isinstance(gotnum, int):
                if not self.right is None:
                    add_num(num, indices, self.right)
                    return True
                self.leftint = indices
                return False
            if len(indices) == 4 and self.right is None:
                    left, self.right = gotnum
                    set_num(num, indices, 0)
                    if not self.leftint is None: 
                        add_num(num,self.leftint,left)
                    return False
        def endstate(self):
            return not self.right is None
    while explode().process(num) or split().process(num):
        pass

init_inputs()
result = inputs.pop(0)
while inputs:
    result = [result, inputs.pop(0)] 
    reduce(result)    

print(magnitute(result))


def next_pair(pair, limitidx):
    if not pair:
        pair.extend([0, 1])
    else:
        pair[1] += 1 if pair[0] != pair[1] + 1 else 2
        if(pair[1] >= limitidx):
            pair[0] += 1
            pair[1] = 0
        if(pair[0] >= limitidx):
            return False
    return True

init_inputs()
pairidx = []
maxresult = 0
while next_pair(pairidx, len(inputs)):
    result = [deepcopy(inputs[pairidx[0]]), deepcopy(inputs[pairidx[1]])]
    reduce(result)
    maxresult = max((magnitute(result),maxresult))
print(maxresult)