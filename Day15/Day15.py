from functools import reduce
inputmap = [[int(num) for num in numstr.rstrip()] for numstr in open('input.txt')]

def process(inputmap):
    max = sum(reduce(lambda la, lb : la + lb, inputmap))
    waymap = [[max for num in row ] for row in inputmap]
    waymap[0][0] = 0
    processqueue = [(0,0)]
    def adjs_x_y(x, y):
        return [ (ax, ay) for ax, ay in [(x-1,y), (x+1,y), (x,y-1), (x,y+1)] if 
                ax >= 0 and ax < len(inputmap) and
                ay >= 0 and ay < len(inputmap[0]) ]
    def process_impl(x,y):
        for (ax, ay) in adjs_x_y(x,y):
            pathval = waymap[x][y] + inputmap[ax][ay]
            if (waymap[ax][ay] > pathval and 
                waymap[-1][-1] > pathval):
                waymap[ax][ay] = pathval
                processqueue.append((ax,ay))
            processqueue.sort(key=lambda p: waymap[p[0]][p[1]])
    while processqueue:
        process_impl(*processqueue.pop(0))
    print(waymap[-1][-1])

process(inputmap)

factor = 5
inputmap2 = []
for i in range(0,factor):
    inputmap2 += [row * factor for row in inputmap]
for i in range(0, len(inputmap2)):
    for j in range(0,len(inputmap2[i])):
        inputmap2[i][j] += i//len(inputmap) + j//len(inputmap)
        if inputmap2[i][j] >= 10:
            inputmap2[i][j] -= 9

process(inputmap2)