inputtxt = open('input.txt')

SIZE = 5

draws = [ int(inputdraw) for inputdraw in inputtxt.readline().split(',') ]
boards = []
for lineraw in inputtxt:
     line = lineraw.rstrip().lstrip()
     if len(line) > 0:
         nums = [ (int(inputboardnum), False) for inputboardnum in line.split(' ') if len(inputboardnum) > 0]
         if len(boards) == 0 or len(boards[-1]) == SIZE*SIZE:
             boards.append(nums)
         else:
             boards[-1] += nums

def get_row(board, row):
    return board[row*SIZE:(row+1)*SIZE]

def get_col(board, col):
    return board[col::SIZE]

def get_win_index(boards):
    for index in range(0, len(boards)):
        for lineindex in range(0, SIZE):
            if all(checked for (num, checked) in get_row(boards[index], lineindex)) or all(checked for (num, checked) in get_col(boards[index], lineindex)):
                return index
    return -1

def mark_boards(boards, draw):
    for board in boards:
        if (draw, False) in board:
            board[board.index((draw, False))] = (draw, True)

def calc_board(board, draw):
    return draw * sum([unchecked for (unchecked, ischecked) in board if ischecked == False])

#part 1
winindex = -1 
while winindex < 0:
    draw = draws.pop(0)
    mark_boards(boards, draw)
    winindex = get_win_index(boards)    

result1 = calc_board(boards[winindex], draw)
print(result1)

#part 2
boards.pop(winindex)
winindex = -1 
while winindex < 0 or len(boards) > 1:
    while winindex >= 0:
        boards.pop(winindex)
        winindex = get_win_index(boards)
    draw = draws.pop(0)
    mark_boards(boards, draw)
    winindex = get_win_index(boards) 

result2 = calc_board(boards[0], draw)
print(result2)