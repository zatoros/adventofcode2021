import re

ifile = open('input.txt')

formula = tuple(ifile.readline().rstrip())
ifile.readline()
rules = {tuple(key) : val for key, val in [re.search('(\w+) -> (\w)',line).groups()[:2] for line in ifile]}
counts = {poly : formula.count(poly) for poly in set(rules.values())}

def inc_pairs(pairdict, pairs, val=1):
    for pair in pairs:
        pairdict.setdefault(pair,0)
        pairdict[pair] += val

def process_pairs(pairs, counts):
    newpairs = {}
    for pair, val in pairs.items():
        counts[rules[pair]] += val
        inc_pairs(newpairs, ((pair[0],rules[pair]),(rules[pair],pair[1])), val)
    return newpairs

def solve(iters, counts):
    pairs = {}
    inc_pairs(pairs, (formula[pairnum:pairnum+2] for pairnum in range(0,len(formula)-1)))
    for iter in range(0,iters):
        pairs = process_pairs(pairs, counts)
    print(max(counts.values()) - min(counts.values()))

solve(10, counts.copy())
solve(40, counts)