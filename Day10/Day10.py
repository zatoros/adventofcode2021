import functools

openings = '([{<'
closeings = ')]}>'
closeings2openings = dict(zip(closeings,openings))
grades1 = dict(zip(closeings,(3,57,1197,25137)))
grades2 = dict(zip(openings,(1,2,3,4)))

result1 = 0
result2 = []
for inputs in (line.rstrip() for line in open('input.txt')):
    brackets = {bracket : 0 for bracket in openings}
    foundbug = False
    while not foundbug:
            for index in range(1, len(inputs)):
                if inputs[index] in closeings and inputs[index-1] in openings:
                    if closeings2openings[inputs[index]] == inputs[index-1]:
                        inputs = inputs[:index-1] + inputs[index+1:]
                    else:
                        result1 += grades1[inputs[index]]
                        foundbug = True
                    break
            else:
                break
    if not foundbug:
        result2temp = functools.reduce(lambda x, y: x*5 + grades2[y], inputs[::-1], 0)
        result2.append(result2temp)


print(result1)

result2.sort()
print(result2[len(result2)//2])