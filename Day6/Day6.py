population = [ int(input) for input in open('input.txt').readline().split(',') ]
population = { day : population.count(day) for day in range(0,9) }

days = 0
result1 = {}
while days < 256:
    if days == 80: result1 = population.copy()
    newfishes = population[0]
    for i in range(0,8) : population[i] = population[i+1]        
    population[6] += newfishes
    population[8] = newfishes
    days += 1

def count_result(result):
    return sum([fish for day, fish in result.items()])

print(count_result(result1))
print(count_result(population))