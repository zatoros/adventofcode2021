
inputs = [[int(input) for input in line.rstrip()] for line in open('input.txt')]

def inc_input():
    for x in range(0, len(inputs)):
        for y in range(0, len(inputs[0])):
            inputs[x][y] += 1

def flash():
    flashset = set()
    def is_valid_pos(x,y):
        return (x >= 0 and x < len(inputs) and
                y >= 0 and y < len(inputs[0]))
    def valid_pos_adj(x,y):
        return [(nx, ny) for nx, ny in [(x-1,y-1), (x,y-1), (x+1,y-1), 
                                        (x-1,y),            (x+1,y), 
                                        (x-1,y+1), (x,y+1), (x+1,y+1)]
                if is_valid_pos(nx,ny)]
    def flash_impl(x,y):
        inputs[x][y] = 0
        flashset.add((x,y))
        for (nx, ny) in valid_pos_adj(x,y):   
            if (nx,ny) in flashset: continue
            inputs[nx][ny] +=1
            if inputs[nx][ny] > 9:
                flash_impl(nx,ny)
    for x in range(0, len(inputs)):
        for y in range(0, len(inputs[0])):
            if inputs[x][y] > 9:
                flash_impl(x,y)
    return len(flashset)

def simul_flash():
    return all([not bool(input) for row in inputs for input in row])

result1 = 0
step = 0
while not simul_flash():
    inc_input()
    flashes = flash()
    result1 += flashes if step < 100 else 0
    step += 1

print(result1)
print(step)