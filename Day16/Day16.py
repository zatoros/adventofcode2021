from functools import reduce

class InputParser:
    VERSION_SIZE = 3
    TYPEID_SIZE = 3
    HEADER_SIZE = 6
    LASTGROUPFLAG_SIZE = 1
    LITERALGROUP_SIZE = 4
    LENGTHTYPE_ID = 1
    PACKETBITS_SIZE = 15
    PACKETSNUM_SIZE = 11

    def extract_bits(self, size):
        result, self.input = self.input[:size], self.input[size:]
        return result

    def __init__(self, filename):
        self.input = ''.join(['{0:#010b}'.format(int(inputnum,16))[-4:] 
                              for inputnum in open(filename).readline().rstrip()])        
        self.sumver = 0

    def parse_header(self):
        return (int(self.extract_bits(InputParser.VERSION_SIZE),2),
                int(self.extract_bits(InputParser.TYPEID_SIZE),2))

    def parse_literal(self):
        result = ''
        literalbitsize, padding = 0, 0
        while True:
            lastgroup, literalbits = self.extract_bits(InputParser.LASTGROUPFLAG_SIZE), self.extract_bits(InputParser.LITERALGROUP_SIZE)
            result += literalbits
            if lastgroup == '0':
                literalbitsize = len(result)//4*5+InputParser.HEADER_SIZE
                break
        return (int(result,2), literalbitsize)

    def parse_operator(self, typeid):
        lengthtypeid = self.extract_bits(InputParser.LENGTHTYPE_ID)
        packetbitsize = InputParser.HEADER_SIZE + 1
        results = []
        if lengthtypeid == '0':
            packetbitsnum = int(self.extract_bits(InputParser.PACKETBITS_SIZE),2)
            packetbitsize += packetbitsnum + InputParser.PACKETBITS_SIZE
            processedbits = 0
            while processedbits != packetbitsnum:
                result, bits = self.parse_packet()
                processedbits += bits
                results.append(result)
        else:
            packetsnum = int(self.extract_bits(InputParser.PACKETSNUM_SIZE),2)
            packetbitsize += InputParser.PACKETSNUM_SIZE
            for i in range(0,packetsnum):
                result, bits = self.parse_packet()
                packetbitsize += bits
                results.append(result)

        return (self.calc_val(typeid, results), packetbitsize)

    def calc_val(self, typeid, results):
        if typeid == 0:
            return sum(results)
        elif typeid == 1:
            return reduce(lambda x,y: x*y, results)
        elif typeid == 2:
            return min(results)
        elif typeid == 3:
            return max(results)
        elif typeid == 5:
            return 1 if results[0] > results[1] else 0
        elif typeid == 6:
            return 1 if results[0] < results[1] else 0
        elif typeid == 7:
            return 1 if results[0] == results[1] else 0

    def parse_packet(self):
        version, typeid = self.parse_header()
        self.sumver += version
        if typeid == 4:
            return self.parse_literal()
        else:
            return self.parse_operator(typeid)

    def parse(self):
        while self.input != '0' * len(self.input):
            self.result = self.parse_packet()[0]

    def result1(self):
        return self.sumver

    def result2(self):
        return self.result


parser = InputParser('input.txt')
parser.parse()
print(parser.result1())
print(parser.result2())