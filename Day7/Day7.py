positions = [ int(input) for input in open('input.txt').readline().split(',') ]

result1 = max(positions)*len(positions)
result2 = result1*len(positions)//2

def fuel_cost_1(x,position):
    return abs(x-position)

def fuel_cost_2(x,position):
    return fuel_cost_1(x,position)*(1+fuel_cost_1(x,position))//2

for position in range(min(positions),max(positions)+1):
    result1 = min([sum([fuel_cost_1(x,position) for x in positions]), result1])
    result2 = min([sum([fuel_cost_2(x,position) for x in positions]), result2])

print(result1)
print(result2)