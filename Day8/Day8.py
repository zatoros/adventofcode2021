import re

def parse_input(line):
    parsed = [parsedline.rstrip() for parsedline in 
              re.search('(\w+ )'*10+'\| '+'(\w+ ?)'*4, line).groups()]
    return parsed[:10], parsed[10:]

def add_to_digitmap(digitmap,input, num):
    digitmap[''.join(sorted(input))] = num

def resolve_digits(inputs):
    digitmap = {}
    input2digit = {}
    for input in inputs:
        if len(input) == 2:
            add_to_digitmap(digitmap,input,1)
            input2digit[1] = set(input)
        elif len(input) == 4:
            add_to_digitmap(digitmap,input,4)
            input2digit[4] = set(input)
        elif len(input) == 3:
            add_to_digitmap(digitmap,input,7)
        elif len(input) == 7:
            add_to_digitmap(digitmap,input,8)
    for input in inputs:
        inputset = set(input)
        if len(input) == 5:
            if inputset > input2digit[1]:
                add_to_digitmap(digitmap,input,3)
            elif len(inputset & input2digit[4]) == 2:
                add_to_digitmap(digitmap,input,2)
            else:
                add_to_digitmap(digitmap,input,5)
        elif len(input) == 6:
            if inputset > input2digit[4]:
                add_to_digitmap(digitmap,input,9)
            elif inputset > input2digit[1]:
                add_to_digitmap(digitmap,input,0)
            else:
                add_to_digitmap(digitmap,input,6)

    return digitmap

def count_result(outputs, digitmap):
    result = 0
    for index, output in enumerate(outputs):
        outputsorted = ''.join(sorted(output))
        result += digitmap[outputsorted] * (10** (len(outputs) - index - 1))
    return result

result1 = result2 = 0
for line in open('input.txt'):
    inputs, outputs = parse_input(line)
    result1 += len([unique for unique in outputs if len(unique) in [2,4,3,7]])
    digitmap = resolve_digits(inputs)
    result2 += count_result(outputs, digitmap)

print(result1)
print(result2)