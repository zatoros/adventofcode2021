def count_positive_diffs(numbers):
    result = [ a-b for a,b in zip(numbers[1:], numbers[:-1]) if a-b > 0 ]
    return len(result)

numbers = [int(line.rstrip()) for line in open('input.txt') ]

#part 1
print(count_positive_diffs(numbers))

#part 2
sums3 = [sum(numbers[i-1:i+2]) for i in range(1,len(numbers)-1)]
print(count_positive_diffs(sums3))